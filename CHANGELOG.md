# Git Changelog Demo App

_This is a demo app for Git changelog, a utility tool for generating changelogs. It is free and opensource. :)_

## v0.2 0.1 ( Wed Jun 06 2018 20:35:26 GMT+0200 (Central Europe Daylight Time) )


## Bug Fixes

  - **app**
    - add changelog configuration
  ([a954ff91](https://bitbucket.org/viktor-cloudhorizon/changelog-demo/commits/a954ff911fd48718f5df0c2cc159be6e2810e991))




## Features

  - **app**
    - better config
  ([c9963066](https://bitbucket.org/viktor-cloudhorizon/changelog-demo/commits/c99630662d7f6df03031ff7a16407161727f407a))
    - add feature 2
  ([aed0bc8e](https://bitbucket.org/viktor-cloudhorizon/changelog-demo/commits/aed0bc8e5489eefc62a4ce26fac0e51def503b0f))
    - add feature 1
  ([683dd1c4](https://bitbucket.org/viktor-cloudhorizon/changelog-demo/commits/683dd1c471bd004d5ec75705f05bbd414167a52a))




## Documentation

  - **app**
    - add changelog file
  ([354b469d](https://bitbucket.org/viktor-cloudhorizon/changelog-demo/commits/354b469d1676c72297fd30b2e623fc7e834904f7))





----
